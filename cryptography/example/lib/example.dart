import 'package:cryptography/cryptography.dart';

void _testAes() async {
  var aes = AesCbc.with128bits(macAlgorithm: MacAlgorithm.empty);
  var data = List<int>.filled(32, 0);
  var _password = List<int>.filled(16, 0);
  print(data);
  var key = await aes.newSecretKeyFromBytes(_password);
  print(await key.extractBytes());
  var result =
      await aes.encrypt(data, secretKey: key, nonce: [], usePadding: false);
  print(result.cipherText.length);
  print(result.cipherText.sublist(0, 32));
  print(_password);

  data = result.cipherText;
  aes = AesCbc.with128bits(macAlgorithm: MacAlgorithm.empty);
  var secretBox = SecretBox(data, nonce: _password, mac: Mac.empty);
  data = (await aes.decrypt(secretBox,
      secretKey: SecretKey(_password), usePadding: false));
  print(data);
}

Future<void> main() async {
  final algorithm = AesGcm.with256bits();

  // Generate a random 256-bit secret key
  final secretKey = await algorithm.newSecretKey();

  // Generate a random 96-bit nonce.
  final nonce = algorithm.newNonce();

  // Encrypt
  final clearText = [1, 2, 3];
  final secretBox = await algorithm.encrypt(
    clearText,
    secretKey: secretKey,
    nonce: nonce,
  );
  print('Ciphertext: ${secretBox.cipherText}');
  print('MAC: ${secretBox.mac}');
  _testAes();
}
